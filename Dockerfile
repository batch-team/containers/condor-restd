FROM python:3.6

RUN pip3 install flask flask-restful htcondor gunicorn && \
    pip3 install flask git+https://github.com/htcondor/htcondor-restd.git@e7bbc70

EXPOSE 8000/tcp

CMD ["gunicorn", "--bind", ":8000", "--workers", "3", "condor_restd:app"]
